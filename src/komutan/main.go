package main

import (
	"komutan/handlers"
	"net/http"
)

func main() {
	//http.HandleFunc("/", handlers.ListCategories)
	http.HandleFunc("/", handlers.ListCommands)
	http.HandleFunc("/run", handlers.RunCommands)

	http.ListenAndServe(":8080", nil)
}
