package models

// Option for select tag
type Option struct {
	Label    string
	Value    string
	Selected bool
}

// FormObject for form objects
type FormObject struct {
	Object      string   `json:"Object,omitempty"`
	Type        string   `json:"Type,omitempty"` // text, input, checkbox, radio
	Value       string   `json:"Value,omitempty"`
	Label       string   `json:"Label,omitempty"`
	Maxlength   int      `json:"Maxlength,omitempty"`   // text, input
	Placeholder string   `json:"Placeholder,omitempty"` // text, input
	Required    bool     `json:"Required,omitempty"`
	Size        int      `json:"Size,omitempty"`    // text, input
	Checked     bool     `json:"Checked,omitempty"` // checkbox, radio
	Multiple    bool     `json:"Multiple,omitempty"`
	Options     []Option `json:"Options,omitempty"`
	Style       string   `json:"Style,omitempty"` // css
}

// Form for main form decleration.
type Form struct {
	Description string `json:"Description,omitempty"`
	Application string `json:"Application,omitempty"`
	Category    string `json:"Category,omitempty"`
	FormObject  []FormObject
}
