$(document).ready(function() {

    function ajaxCallRequest(f_method, f_url, f_data) {
      $("#dataSent").val(unescape(f_data));
      var f_contentType = 'application/x-www-form-urlencoded; charset=UTF-8';
      $.ajax({
        url: f_url,
        type: f_method,
        contentType: f_contentType,
        dataType: 'json',
        data: f_data,
        success: function(data) {
          //var jsonResult = JSON.stringify(data);
          //$("#results").val(unescape(jsonResult));
          $("#results").val(unescape(data));
        }
      });
    }
    
    // Add Email (example)
    $(document).on('click', '.btn-add', function(event) {
      event.preventDefault();
      var controlForm = $('.controls');
      var currentEntry = $(this).parents('.entry:first');
      var newEntry = $(currentEntry.clone()).appendTo(controlForm);
      newEntry.find('input').val('');
      controlForm.find('.entry:not(:last) .btn-add')
              .removeClass('btn-add').addClass('btn-remove')
              .removeClass('btn-success').addClass('btn-danger')
              .html('<span class="glyphicon glyphicon-minus"></span>');
              
      var inputs = $('.controls .form-control');
      $.each(inputs, function(index, item) {
        item.name = 'emails[' + index + ']';
      });
    });
    
    // Remove Email
    $(document).on('click', '.btn-remove', function(event) {
      event.preventDefault();
      $(this).parents('.entry:first').remove();
      var inputs = $('.controls .form-control');
      $.each(inputs, function(index, item) {
        item.name = 'emails[' + index + ']';
      });
    });


    // Send Query String (POST/GET)
    $("#sendQueryString").click(function(event) {
      event.preventDefault();
      var form = $('#ajaxForm');
      var method = form.attr('method');
      var url = form.attr('action');
      var data = $(form).serialize();
      console.log(data);
      ajaxCallRequest(method, url, data);
    });
  
    // Send Plain Json
    $("#sendPlainJSon").click(function(event) {
      event.preventDefault();
      var form = $('#ajaxForm');
      var method = form.attr('method');
      var url = form.attr('action');
      var jsonData = {};
      $.each($(form).serializeArray(), function() {
        jsonData[this.name] = this.value;
      });
      var data = JSON.stringify(jsonData);
      console.log(data);
      ajaxCallRequest(method, url, data);
    });
  
    
    // mock! data :)
    $("#defaultData").click(function(event) {
      event.preventDefault();
      $('#firstname').val('Mortadelo');
      $('#lastname').val('Filemon');
      $('#address_street').val('Rua del Percebe 13');
      $('#address_city').val('Madrid');
      $('#address_zip').val('28010');
      $("[name='emails[0]']").val('superintendencia@cia.es');
    });
  
  });  