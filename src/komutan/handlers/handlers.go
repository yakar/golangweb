package handlers

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"komutan/models"
	"net/http"
	"os"
	"os/exec"
	"strings"
)

const jsonDir = "commands/"

func CheckError(err error) {
	if err != nil {
		fmt.Println("ERROR>>", err)
	}
}

func ListCategories(w http.ResponseWriter, r *http.Request) {

	files, err := ioutil.ReadDir(jsonDir)
	CheckError(err)
	var AllCats []models.Form
	for _, file := range files {
		fmt.Println(file)
		// jsonOpen, err := os.OpenFile(jsonDir+file.Name(), os.O_RDONLY, 0666)
		// defer jsonOpen.Close()
		// CheckError(err)

		jsonFile, err := ioutil.ReadFile(jsonDir + file.Name())
		CheckError(err)

		var Cat models.Form
		err = json.Unmarshal(jsonFile, &Cat)
		AllCats = append(AllCats, Cat)
		CheckError(err)
	}
	t, err := template.ParseFiles("templates/categories.html")
	CheckError(err)
	//fmt.Println(AllCats)
	t.Execute(w, AllCats)
}

func ListCommands(w http.ResponseWriter, r *http.Request) {

	f, err := os.Open(jsonDir)
	CheckError(err)

	files, err := f.Readdir(-1)
	f.Close()
	CheckError(err)

	var AllForms []models.Form
	for _, file := range files {
		jsonOpen, err := os.OpenFile(jsonDir+file.Name(), os.O_RDONLY, 0666)
		defer jsonOpen.Close()
		CheckError(err)

		jsonFile, err := ioutil.ReadAll(jsonOpen)
		CheckError(err)

		var Forms models.Form
		err = json.Unmarshal(jsonFile, &Forms)
		// value ve options icin datasource eklenebilmesi
		AllForms = append(AllForms, Forms)
		CheckError(err)
	}
	t, err := template.ParseFiles("templates/form.html")
	CheckError(err)
	//fmt.Println(AllForms)
	t.Execute(w, AllForms)
	fmt.Println("ListCommands runned!")
}

func RunCommands(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()

	app := strings.Join(r.Form["app"], "")
	params := r.Form["params"]

	cmd := exec.Command(app, params...)
	cikti, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Fprintf(w, "%s", err)
	}

	fmt.Fprintf(w, "Form başarıyla gönderildi!")
	fmt.Fprintf(w, "\nOutput:\n%s", cikti)
}
