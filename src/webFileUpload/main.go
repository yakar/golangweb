package main

import (
	"controllers/democontroller"
	"net/http"
)

func main() {
	http.HandleFunc("/demo/index1", democontroller.Index1)
	http.HandleFunc("/demo/singleupload", democontroller.SingleUpload)
	http.HandleFunc("/demo/index2", democontroller.Index2)
	http.HandleFunc("/demo/multipleupload", democontroller.MultipleUpload)
	http.HandleFunc("/demo/index3", democontroller.Index3)
	http.HandleFunc("/demo/runcommand", democontroller.RunCommand)

	http.ListenAndServe(":3000", nil)
}
