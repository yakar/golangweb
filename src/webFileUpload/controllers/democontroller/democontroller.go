package democontroller

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"os/exec"
	"text/template"
)

// Index3 sigle upload file form
func Index3(response http.ResponseWriter, request *http.Request) {
	tmplt, _ := template.ParseFiles("views/democontroller/index3.html")
	tmplt.Execute(response, nil)
}

// RunCommand parametre ile komutu calistirir.
func RunCommand(response http.ResponseWriter, request *http.Request) {
	komut := request.FormValue("komut")

	out, err := exec.Command("sh", "-c", komut).Output()
	if err != nil {
		fmt.Printf("%s", err)
	}
	fmt.Printf("%s", out)

	tmplt, _ := template.ParseFiles("views/democontroller/index3.html")
	tmplt.Execute(response, nil)
}

// Index1 sigle upload file form
func Index1(response http.ResponseWriter, request *http.Request) {
	tmplt, _ := template.ParseFiles("views/democontroller/index1.html")
	tmplt.Execute(response, nil)
}

// SingleUpload Index1 submit to this func
func SingleUpload(response http.ResponseWriter, request *http.Request) {

	// test
	fmt.Printf("%s", request.FormValue("test"))

	request.ParseMultipartForm(5 * 1024 * 1024)
	file, handler, _ := request.FormFile("file")
	defer file.Close()
	fmt.Println("File Name:", handler.Filename)
	fmt.Println("File Size:", handler.Size)

	// save file
	dst, _ := os.Create("./uploads/" + handler.Filename)
	defer dst.Close()
	io.Copy(dst, file)

	tmplt, _ := template.ParseFiles("views/democontroller/index1.html")
	tmplt.Execute(response, nil)
}

// Index2 multiple upload file form
func Index2(response http.ResponseWriter, request *http.Request) {
	tmplt, _ := template.ParseFiles("views/democontroller/index2.html")
	tmplt.Execute(response, nil)
}

// MultipleUpload Index1 submit to this func
func MultipleUpload(response http.ResponseWriter, request *http.Request) {
	request.ParseMultipartForm(5 * 1024 * 1024)
	files := request.MultipartForm.File["files"]
	fmt.Println("Files:", len(files))

	for i, fileHandler := range files {
		fmt.Println("File No:", i)
		fmt.Println("File Name:", fileHandler.Filename)
		fmt.Println("File Size:", fileHandler.Size)

		// save file
		file, _ := files[i].Open()
		defer file.Close()

		dst, _ := os.Create("./uploads/" + fileHandler.Filename)
		defer dst.Close()

		io.Copy(dst, file)

	}

	tmplt, _ := template.ParseFiles("views/democontroller/index2.html")
	tmplt.Execute(response, nil)
}
