package main

import (
	"fmt"
	"log"
	"time"
	"html/template"
	"net/http"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

var (
	id 			int
	username 	string
	password 	string
	createdAt 	time.Time
)

func main()  {

	tmplt := template.Must(template.ParseFiles("register.html"))

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request){
		// eger form post edilmedi ise
		if r.Method != http.MethodPost {
			tmplt.Execute(w, nil)
			return
		}
	})
	http.HandleFunc("/register", func(w http.ResponseWriter, r *http.Request){
		// DB islemleri
		db, err := sql.Open("mysql", "kullanici:parola@(10.10.10.10:3306)/database")
		if err != nil {
			log.Fatal(err)
		}

		if err := db.Ping(); err != nil {
			log.Fatal(err)
		}

		{ // Insert a new user
			username = r.FormValue("username")
			password = r.FormValue("password")
			createdAt = time.Now()
	
			result, err := db.Exec(`INSERT INTO users (username, password, created_at) VALUES(?, ?, ?)`, username, password, createdAt)
			if err != nil {
				log.Fatal(err)
			}
	
			id, err := result.LastInsertId()
			fmt.Println(id)

			tmplt.Execute(w, struct{ Success bool }{true})
		}
	})

	http.ListenAndServe(":8080", nil)

	// { // Create a new table
	// 	query := `
	// 		CREATE TABLE users (
	// 			id INT AUTO_INCREMENT,
	// 			username TEXT NOT NULL,
	// 			password TEXT NOT NULL,
	// 			created_at DATETIME,
	// 			PRIMARY KEY (id)
	// 		)
	// 	`
	// 	if _, err := db.Exec(query); err != nil {
	// 		log.Fatal(err)
	// 	}
	// }



	// { //Query a single user

	// 	query := "SELECT id, username, password, created_at FROM users WHERE  id = ?"
	// 	if err := db.QueryRow(query, 1).Scan(&id, &username, &password, &createdAt); err != nil {
	// 		log.Fatal(err)
	// 	}

	// 	fmt.Println(id, username, password, createdAt)
	// }

	// { //Query all users
	// 	type user struct {
	// 		id			int
	// 		username	string
	// 		password	string
	// 		createdAt	time.Time
	// 	}

	// 	rows, err := db.Query(`SELECT id, username, password, created_at FROM users`)
	// 	if err != nil {
	// 		log.Fatal(err)
	// 	}
	// 	defer rows.Close()

	// 	var users []user
	// 	for rows.Next() {
	// 		var u user

	// 		err := rows.Scan(&u.id, &u.username, &u.password, &u.createdAt)
	// 		if err != nil {
	// 			log.Fatal(err)
	// 		}
	// 		users = append(users, u)
	// 	}
	// 	if err := rows.Err(); err != nil {
	// 		log.Fatal(err)
	// 	}
	// }

	// {
	// 	_, err := db.Exec(`DELETE FROM users WHERE id = ?`, 1)
	// 	if err != nil {
	// 		log.Fatal(err)
	// 	}
	// }
}