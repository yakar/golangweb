package httpserver

import (
	"fmt"
	"net/http"
)

func main() {
	// GET : r.URL.Query().Get("variable")
	// POST: r.FormValue("email")
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Welcome  to my website!")
	})

	// JavaScript, CSS, Images: /static/
	fs := http.FileServer(http.Dir("static/"))
	http.Handle("/static/", http.StripPrefix("/static", fs))

	http.ListenAndServe(":8080", nil)
}
